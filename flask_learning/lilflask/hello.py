from flask import Flask, url_for, render_template, abort, redirect
app = Flask(__name__)

@app.route('/')
def index():
    return redirect(url_for('login'))

@app.route('/login')
def login():
    abort(401)
    this_is_never_executed()

@app.errorhandler(404)
def page_not_found(error):
  return render_template("page_not_found.html"), 404

@app.route("/hello")
@app.route("/hello/<name>")
def hello(name = None):
  return render_template("hello.html", name = name)

@app.route("/user/<username>")
def show_user_profile(username):
  # Show the user profile for that user.
  return "User %s is Jeremy's friend" % username

@app.route("/post/<int:post_id>")
def show_post(post_id):
  # Show the post with the given id, the id is an integer.
  return "Post %d" % post_id

# @app.route("/post/<string:post_id>")
# def show_post(post_id):
#   # Should have been handled by the integer version, return error.
#   return "This is not a legitimate post id. Use integers plz thx."

@app.route("/lots/of/urls")
def show_lots_of_urls():
  result = ""
  result += url_for("index") + "\n"
  result += url_for("hello") + "\n"
  result += url_for("show_user_profile", username = "Don Jo")
  return result

# @app.route("/login", methods = ["GET", "POST"])
# def login():
#   if request.method == "POST":
#     do_the_login()
#   else:
#     show_the_login_form()

# def do_the_login():
#   return "Funk town, logging you in."

# def show_the_login_form():
#   return "You trying to log in yet man?"

if __name__ == "__main__":
  app.run(debug=True)
