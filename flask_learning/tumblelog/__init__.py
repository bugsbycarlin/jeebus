from flask import Flask
from flask.ext.mongoengine import MongoEngine

app = Flask(__name__)
app.config["MONGODB_SETTINGS"] = {"DB": "jaunty"}
app.config["SECRET_KEY"] = "\xa5\x97\x90^O`\xdb\xeaW\x0e\x18\xc1\xee\x9bN\xe3\xfdu\r~\xe8\x8d\xa1\x11"

db = MongoEngine(app)

def register_blueprints(app):
  # Prevents circular imports
  # BUGSBY: I don't like that at all.
  from tumblelog.views import posts
  from tumblelog.admin import admin
  app.register_blueprint(posts)
  app.register_blueprint(admin)

register_blueprints(app)

if __name__ == "__main__":
  app.run()