import datetime

from flask.ext.mongoengine import *
from mongoengine.fields import *
from mongoengine.errors import DoesNotExist

class Data(Document):
  latitude = FloatField(required = True)
  longitude = FloatField(required = True)
  speed = FloatField(required = True)
  heading = FloatField(required = True)
  route_slug = StringField(required = True)
  user_id = StringField(required = True)
  timestamp = DateTimeField(required = True)  # this comes from the user, not from us


class Route(Document):
  name = StringField(max_length = 255, required = True)
  region = StringField(max_length = 255, required = True)
  slug = StringField(max_length = 255, required = True)
  stops = ListField(EmbeddedDocumentField("Stop"))
  lines = ListField(EmbeddedDocumentField("Line"))

  def make_public(self):
    return {
      "name": self.name,
      "region": self.region,
      "slug": self.slug,
      "stops": [stop.make_public() for stop in self.stops],
      "lines": [line.make_public() for line in self.lines],
    }

  def duplicate(self, new_slug, overwrite):
    try:
      dupe_route = Route.objects.get(slug = new_slug)
    except DoesNotExist:
      dupe_route = None

    if not dupe_route:
      new_route = Route()
      new_route.name = self.name
      new_route.region = self.region
      new_route.slug = new_slug
      new_route.stops = [stop.duplicate() for stop in self.stops]
      new_route.lines = [line.duplicate() for line in self.lines]
      return new_route
    elif overwrite == False:
      return None
    elif overwrite == True:
      dupe_route.name = self.name
      dupe_route.region = self.region
      dupe_route.stop = [stop.duplicate() for stop in self.stops]
      dupe_route.lines = [line.duplicate() for line in self.lines]
      return dupe_route
    

class Stop(Document):
  route_slug = StringField(required = True)
  latitude = FloatField(required = True)
  longitude = FloatField(required = True)
  stop_id = IntField(required = True)
  direction = StringField(max_length = 5, required = True)

  def make_public(self):
    return {
      "route_slug": self.route_slug,
      "latitude": self.latitude,
      "longitude": self.longitude,
      "stop_id": self.stop_id,
      "direction": self.direction
    }

  def duplicate(self):
    new_stop = Stop()
    new_stop.route_slug = self.route_slug
    new_stop.latitude = self.latitude
    new_stop.longitude = self.longitude
    new_stop.stop_id = self.stop_id
    new_stop.direction = self.direction
    return new_stop


class Line(Document):
  route_slug = StringField(required = True)
  start_latitude = FloatField(required = True)
  start_longitude = FloatField(required = True)
  end_latitude = FloatField(required = True)
  end_longitude = FloatField(required = True)

  def make_public(self):
    return {
      "route_slug": self.route_slug,
      "start_latitude": self.start_latitude,
      "start_longitude": self.start_longitude,
      "end_latitude": self.end_latitude,
      "end_longitude": self.end_longitude,
    }

  def duplicate(self):
    new_line = Line()
    new_line.route_slug = self.route_slug
    new_line.start_latitude = self.start_latitude
    new_line.start_longitude = self.start_longitude
    new_line.end_latitude = self.end_latitude
    new_line.end_longitude = self.end_longitude
    return new_line
