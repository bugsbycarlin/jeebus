from bson import ObjectId
from flask import Flask, abort, jsonify, make_response, request, current_app
from functools import wraps

from common.models import Route, Stop, Line
from routecorder import routecorder_app

secret = "OHWEGFNOSUDGVWPSLSDERETU"

# https://gist.github.com/farazdagi/1089923
# but unnecessary
def support_jsonp(f):
    """Wraps JSONified output for JSONP"""
    @wraps(f)
    def decorated_function(*args, **kwargs):
        callback = request.args.get('callback', False)
        if callback:
            content = str(callback) + '(' + str(f().data) + ')'
            return current_app.response_class(content, mimetype='application/json')
        else:
            return f(*args, **kwargs)
    return decorated_function


@routecorder_app.route("/api/v0.0/routecorder", methods = ["GET"])
@support_jsonp
def get_routes():
  routes = Route.objects
  output = {}
  output["routes"] = []
  for route in routes:
    output["routes"].append(route.make_public())
  return jsonify(output)


@routecorder_app.route("/api/v0.0/routecorder/add_route", methods = ["POST"])
def add_route():
  if not request.json or not "secret" in request.json or request.json["secret"] != secret:
    abort(404)
  
  if not "name" in request.json or not "region" in request.json or not "slug" in request.json:
    abort(400)
  
  route = Route()
  route.name = request.json["name"]
  route.region = request.json["region"]
  route.slug = request.json["slug"]
  route.save()
  return jsonify(route.make_public())


@routecorder_app.route("/api/v0.0/routecorder/<string:slug>/duplicate_route", methods = ["POST"])
def duplicate_route(slug):
  if not request.json or not "secret" in request.json or request.json["secret"] != secret:
    abort(404)

  if not "overwrite" in request.json or not "new_slug" in request.json:
    abort(400)

  old_route = Route.objects.get_or_404(slug = slug)

  route = old_route.duplicate(request.json["new_slug"], request.json["overwrite"])
  if route:
    route.save()
    return jsonify(route.make_public())
  else:
    return make_response(jsonify({"error": "Attempting to add a duplicate entry."}), 400)


@routecorder_app.route("/api/v0.0/routecorder/<string:slug>", methods = ["GET"])
def get_route(slug):
  route = Route.objects.get_or_404(slug = slug)
  return jsonify(route.make_public())


@routecorder_app.route("/api/v0.0/routecorder/<string:slug>/add_stop", methods = ["POST"])
def add_stop(slug):
  route = Route.objects.get_or_404(slug = slug)

  if not request.json or not "secret" in request.json or request.json["secret"] != secret:
    abort(404)

  if not "latitude" in request.json or not "longitude" in request.json:
    abort(400)

  stop = Stop()
  stop.route_slug = slug

  stop.latitude = request.json["latitude"]
  stop.longitude = request.json["longitude"]

  route.stops.append(stop)
  route.save()

  return jsonify(stop.make_public())


@routecorder_app.route("/api/v0.0/routecorder/<string:slug>/save_stops", methods = ["POST"])
def save_stops(slug):
  route = Route.objects.get_or_404(slug = slug)

  if not request.json or not "secret" in request.json or request.json["secret"] != secret:
    abort(404)

  if not "stops" in request.json:
    abort(400)

  print request.json["lines"]

  route.stops = []
  for item in request.json["stops"]:
    if item:
      stop = Stop()
      stop.route_slug = slug
      stop.latitude = item[0]
      stop.longitude = item[1]
      route.stops.append(stop)

  route.lines = []
  for item in request.json["lines"]:
    if item and len(item) == 4: 
      line = Line()
      line.route_slug = slug
      line.start_latitude = item[0]
      line.start_longitude = item[1]
      line.end_latitude = item[2]
      line.end_longitude = item[3]
      route.lines.append(line)

  route.save()

  return jsonify({"result": True})




@routecorder_app.route("/api/v0.0/routecorder/<string:slug>/delete_stops", methods = ["DELETE"])
def delete_stops(slug):
  # this should have the secret too, right?
  route = Route.objects.get_or_404(slug = slug)

  route.stops = []
  route.save()

  return jsonify({"result": True})


@routecorder_app.errorhandler(404)
def not_found(error):
  return make_response(jsonify({"error": "Not foooooound"}), 404)


@routecorder_app.errorhandler(400)
def bad_request(error):
  return make_response(jsonify({"error": "Bad request, duder"}), 400)
