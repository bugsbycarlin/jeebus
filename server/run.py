from flask import Flask
from flask.ext.mongoengine import MongoEngine

app = Flask(__name__)

app.config["MONGODB_SETTINGS"] = {"DB": "busby"}

db = MongoEngine(app)

import common
import busby
import routecorder

import busby.actions
import routecorder.actions

app.register_blueprint(routecorder.routecorder_app)
app.register_blueprint(busby.busby_app)

if __name__ == "__main__":
  #print app.url_map
  app.run(debug = True, host = "0.0.0.0")
