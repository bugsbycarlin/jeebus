from bson import ObjectId
from flask import Flask, abort, jsonify, make_response, request, current_app
from functools import wraps

from common.models import Route, Data
from busby import busby_app

import datetime

secret = "OHWEGFNOSUDGVWPSLSDERETU"

# https://gist.github.com/farazdagi/1089923
# but unnecessary
def support_jsonp(f):
    """Wraps JSONified output for JSONP"""
    @wraps(f)
    def decorated_function(*args, **kwargs):
        callback = request.args.get('callback', False)
        if callback:
            content = str(callback) + '(' + str(f().data) + ')'
            return current_app.response_class(content, mimetype='application/json')
        else:
            return f(*args, **kwargs)
    return decorated_function


@busby_app.route("/api/v0.0/busby", methods = ["GET"])
@support_jsonp
def get_routes():
  routes = Route.objects
  output = {}
  output["routes"] = []
  for route in routes:
    output["routes"].append(route.slug)
  return jsonify(output)


@busby_app.route("/api/v0.0/busby/<string:slug>", methods = ["GET"])
@support_jsonp
def get_route(slug):
  route = Route.objects.get_or_404(slug = slug)
  return jsonify(route.make_public())


@busby_app.route("/api/v0.0/busby/data", methods = ["POST"])
def add_route():
  if not request.json or not "secret" in request.json or request.json["secret"] != secret:
    abort(404)

  if (not "latitude" in request.json
      or not "longitude" in request.json
      or not "speed" in request.json
      or not "heading" in request.json
      or not "route_slug" in request.json
      or not "user_id" in request.json
      or not "timestamp" in request.json):
    abort(400)

  datum = Data()
  datum.user_id = request.json["user_id"]
  datum.route_slug = request.json["route_slug"]
  datum.timestamp = datetime.datetime.strptime(request.json["timestamp"], "%m/%d/%y %H:%M:%S")
  datum.latitude = request.json["latitude"]
  datum.longitude = request.json["longitude"]
  datum.speed = request.json["speed"]
  datum.heading = request.json["heading"]
  datum.save()
  return make_response("", 200)


@busby_app.errorhandler(404)
def not_found(error):
  return make_response(jsonify({"error": "Not foooooound"}), 404)


@busby_app.errorhandler(400)
def bad_request(error):
  return make_response(jsonify({"error": "Bad request, duder"}), 400)
