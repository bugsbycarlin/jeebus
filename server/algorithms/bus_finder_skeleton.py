#
# This script reads passenger data for a particular route, and calculates the locations of buses.
#
# Copyright 2014, Matthew Carlin and Luke Mizuhashi
#
# It is meant to be run frequently to update bus location. Hopefully we can run this once every few seconds per route.
#
# Algorithm (draft):
#

import math
import pymongo
import time

from pymongo import MongoClient


# constants
EARTH_RADIUS = 6371 #km
SPEED_FACTOR = 10.0


# calculate great circle distance between two lat/lng pairs. this is in KM
def haversine_distance(lat1, lng1, lat2, lng2):
  lat1 = math.radians(lat1)
  lng1 = math.radians(lng1)
  lat2 = math.radians(lat2)
  lng2 = math.radians(lng2)

  dlat = lat2 - lat1
  dlng = lng2 - lng1

  a = math.sin(dlat / 2.0) ** 2 + math.cos(lat1) * math.cos(lat2) * (math.sin(dlng / 2.0) ** 2)
  c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
  d = c * EARTH_RADIUS

  return d


client = MongoClient()

db = client["busby"]

# measure the execution time
start_time = time.time()

# how many data points are there?
print "%d data points" % db.data.count()


per_passenger_data = {}

# get the route
route = db.route.find_one({"slug": "LA-720"})
#for line in route["lines"]:
#  print line["start_longitude"]
#  print line["start_latitude"]

# add each data point to the per_passenger_data
for datum in db.data.find():
  user_id = datum["user_id"]
  if user_id in per_passenger_data:
    per_passenger_data[user_id] += [datum]
  else:
    per_passenger_data[user_id] = [datum]

# how many passengers
print "%d passengers" % len(per_passenger_data.keys())

# what the fuck.
# ignore this method.
# it prints the number of passengers with 1 data point,
# 2 data points, 3 data points, etc.
def print_point_counts(per_passenger_data):
  histogram = {}
  max_num_points = 0

  for user_id in per_passenger_data.keys():
    num_points = len(per_passenger_data[user_id])
    if num_points in histogram:
      histogram[num_points] += 1
    else:
      histogram[num_points] = 1
    if num_points >= max_num_points:
      max_num_points = num_points

  for num_points in range(1, max_num_points + 1):
    num_passengers = 0
    if num_points in histogram:
      num_passengers = histogram[num_points]
    print "%d points: %d passengers" % (num_points, num_passengers)


print_point_counts(per_passenger_data)

# print the calculated speed of each data point. this is just a check to see if I've got it right.
# Note the calculated values will differ from the simulated values, because I introduced noise
# into the simulator's GPS reporting.
passenger_expected_location = {}

for user_id in per_passenger_data.keys():
  data = per_passenger_data[user_id]
  # ignore people with less than 3 data points
  if len(data) >= 3:
    # sort the data by timestamp
    data.sort(key = lambda datum: datum["timestamp"])
    last_reported_speed = -1
    for i in range(0, len(data) - 1):
      d1 = data[i]
      d2 = data[i + 1]
      delta = d2["timestamp"] - d1["timestamp"]
      time_diff = delta.seconds + delta.microseconds/1E6
      distance = haversine_distance(d1["latitude"], d1["longitude"], d2["latitude"], d2["longitude"])
      speed = 3600.0 * distance / (time_diff * SPEED_FACTOR)
      last_reported_speed = speed
      print "%s: %f kph; simulator thinks %f kph" % (user_id, speed, d2["speed"])

    last_reported_point = data[-1]

    # now calculate estimated location with dead reckoning. still working on this.

# measure the execution time
end_time = time.time()
print "Finished. The process took %f seconds." % (end_time - start_time)