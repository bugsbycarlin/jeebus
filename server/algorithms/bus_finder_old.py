#
# This script reads passenger data for a particular route, and calculates the locations of buses.
#
# Copyright 2014, Matthew Carlin and Luke Mizuhashi
#
# It is meant to be run frequently to update bus location. Hopefully we can run this once every few seconds per route.
#
# Algorithm (draft):
#


import math
import pymongo
import time

EARTH_RADIUS = 6371 #km
SPEED_FACTOR = 10.0

from pymongo import MongoClient

# calculate great circle distance between two lat/lng pairs. this is in KM
def haversine_distance(lat1, lng1, lat2, lng2):
  lat1 = math.radians(lat1)
  lng1 = math.radians(lng1)
  lat2 = math.radians(lat2)
  lng2 = math.radians(lng2)

  dlat = lat2 - lat1
  dlng = lng2 - lng1

  a = math.sin(dlat / 2.0) ** 2 + math.cos(lat1) * math.cos(lat2) * (math.sin(dlng / 2.0) ** 2)
  c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
  d = c * EARTH_RADIUS

  return d

# find the point in a series of line segments that most closely matches the given point,
# then return both the line segment and the closest point
def find_segment(point, lines):


client = MongoClient()

db = client["busby"]

start_time = time.time()

print "%d data points" % db.data.count()

per_passenger_data = {}

route = db.route.find_one({"slug": "LA-720"})
#for line in route["lines"]:
#  print line["start_longitude"]
#  print line["start_latitude"]
#print route
#exit(1)

for datum in db.data.find():
  user_id = datum["user_id"]
  if user_id in per_passenger_data:
    per_passenger_data[user_id] += [datum]
  else:
    per_passenger_data[user_id] = [datum]
  #print datum

print "%d passengers" % len(per_passenger_data.keys())

data_number_buckets = {}
avg_data = 0
max_data_points = 0

for user_id in per_passenger_data.keys():
  num = len(per_passenger_data[user_id])
  if num in data_number_buckets:
    data_number_buckets[num] += 1
  else:
    data_number_buckets[num] = 1
  if num >= max_data_points:
    max_data_points = num
  avg_data += num

avg_data /= float(len(per_passenger_data.keys()))

print "Avg num data points: %f" % avg_data
for num in range(0, max_data_points + 1):
  print_val = 0
  if num in data_number_buckets:
    print_val = data_number_buckets[num]
  print "%d points: %d passengers" % (num, print_val)

passenger_expected_location = {}

for user_id in per_passenger_data.keys():
  data = per_passenger_data[user_id]
  if len(data) >= 3:
    data.sort(key = lambda datum: datum["timestamp"])
    last_speed = -1
    for i in range(0, len(data) - 1):
      d1 = data[i]
      d2 = data[i + 1]
      delta = d2["timestamp"] - d1["timestamp"]
      time_diff = delta.seconds + delta.microseconds/1E6
      distance = haversine_distance(d1["latitude"], d1["longitude"], d2["latitude"], d2["longitude"])
      speed = 3600.0 * distance / (time_diff * SPEED_FACTOR)
      last_speed = speed
      print "%s: %f kph; simulator thinks %f kph" % (user_id, speed, d2["speed"])

    last_point = data[-1]

    segment, closest_point = find_segment(last_point, route["lines"])

    # distance_to_go = SPEED_FACTOR * dt * self.speed / 3600.0
    
    #   while distance_to_go > 0:
    #     segment_remaining_distance = haversine_distance(self.latitude,
    #         self.longitude,
    #         route.lines[self.line_segment_number]["end_latitude"],
    #         route.lines[self.line_segment_number]["end_longitude"])

    #     if segment_remaining_distance > distance_to_go:
    #       # move the bus along the segment for the appropriate distance
    #       fractional_distance = distance_to_go / segment_remaining_distance
    #       self.latitude += fractional_distance * (route.lines[self.line_segment_number]["end_latitude"] - self.latitude)
    #       self.longitude += fractional_distance * (route.lines[self.line_segment_number]["end_longitude"] - self.longitude)
    #       distance_to_go = 0
    #     else:
    #       distance_to_go -= segment_remaining_distance
    #       next_segment_number = self.line_segment_number + 1
    #       if next_segment_number >= len(route.lines):
    #         next_segment_number = 0
    #       self.line_segment_number = next_segment_number
    #       self.latitude = route.lines[self.line_segment_number]["start_latitude"]
    #       self.longitude = route.lines[self.line_segment_number]["start_longitude"]


end_time = time.time()
print "Finished. The process took %f seconds." % (end_time - start_time)