
import datetime
import json
import math
import os
import random
import string
import threading
import time
import urllib2

from pyglet.gl import *
from pyglet import font
from pyglet import clock
from pyglet import image
from pyglet.window import key
from pyglet.window import mouse

from route import *

WIDTH = 1024
HEIGHT = 700
MAX_BUSES = 12
BUS_SPEED = 60 # kph
BUS_CAPACITY = 70
BUS_STOP_TIME = 20 # seconds
SPEED_FACTOR = 10
EARTH_RADIUS = 6371 #km
PASSENGER_DRIP_RATE = 1 / 3.0
PASSENGER_PING_RATE = 10
SEND_GPS = True
STOP_TRANSMITTING_RATE = 1/10000.0
RESTART_TRANSMITTING_RATE = 1/15000.0

MODE_TARGET = 0
MODE_STOP = 1
MODE_BUS = 2
MODE_EGRESS = 3
MODE_FINISHED = 4


# very roughly, 3 ten thousands in lat or lon around LA is like 30 or 40 feet


# calculate great circle distance between two lat/lng pairs. this is in KM
def haversine_distance(lat1, lng1, lat2, lng2):
  lat1 = math.radians(lat1)
  lng1 = math.radians(lng1)
  lat2 = math.radians(lat2)
  lng2 = math.radians(lng2)

  dlat = lat2 - lat1
  dlng = lng2 - lng1

  a = math.sin(dlat / 2.0) ** 2 + math.cos(lat1) * math.cos(lat2) * (math.sin(dlng / 2.0) ** 2)
  c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
  d = c * EARTH_RADIUS

  return d

#
# Start2DDraw performs the OpenGL setup necessary
# to do 2D drawing. It pushes the current (3D) setup
# onto the stack and sets up 2D.
# 
def Start2DDraw(width, height):
  glMatrixMode(GL_PROJECTION)
  glPushMatrix()
  glLoadIdentity()
  gluOrtho2D(0, width, 0, height)
  glMatrixMode(GL_MODELVIEW)
  glPushMatrix()
  glLoadIdentity()
  glPushAttrib(GL_LIGHTING)
  glDisable(GL_LIGHTING)
  glEnable(GL_BLEND)
  glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA)
  glDisable(GL_DEPTH_TEST)
  glColor3f(1,1,1)

#
# End2DDraw is meant as a bookend to Start2DDraw.
# It pops the 2D setup from the stack, returning OpenGL
# to the previous (3D) setup.
#
def End2DDraw():
  glEnable(GL_DEPTH_TEST)
  #glDisable(GL_BLEND)
  glPopAttrib()
  glPopMatrix()
  glMatrixMode(GL_PROJECTION)
  glPopMatrix()
  glMatrixMode(GL_MODELVIEW)


stop_image = image.load("stop_sign.png")
bus_image = image.load("school_bus.png")
passenger_image = image.load("passenger.png")
passenger_egress_image = image.load("passenger_egress.png")
info_walking = image.load("info_walking.png")
info_waiting = image.load("info_waiting.png")
info_riding = image.load("info_riding.png")
info_finished = image.load("info_finished.png")
info_not_transmitting = image.load("info_not_transmitting.png")


chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
def user_id_generator():
  return "".join(random.choice(chars) for x in range(0,7))


class Label(object):
  
  def __init__(self, x = 0, y = 0, fontName ='Arial', fontSize = 14, text='', myColor=(0,0,0,1)):
    
    self.x = x
    self.y = y
    
    self.Visible = True
    
    self.color = myColor

    self.text = text
    
    self.ft = font.load(fontName, fontSize,bold=True)
    self.Text = font.Text(self.ft,color=myColor)
    
  def draw(self):
    if(self.Visible):   
      self.Text.text = self.text
      self.Text.x = self.x
      self.Text.y = self.y
      self.Text.draw()


class Bus(object):

  def __init__(self, _id, stops, route):
    self.speed = random.randint(int(BUS_SPEED * 0.9), int(BUS_SPEED * 1.1))
    #self.passengers = []
    self.passenger_count = 0

    self.line_segment_number = random.randint(0, len(route.lines) - 1)
    self.latitude = route.lines[self.line_segment_number]["start_latitude"]
    self.longitude = route.lines[self.line_segment_number]["start_longitude"]

    self.id = _id

    self.stopped = False
    self.stop_time = 0
    self.next_stop_id = -1

    self.label = Label(10, 570, "Arial", 10, "", (0.3, 0.3, 0.9, 1.0))

  def update(self, route, stops, dt):
    direction = "w"
    if route.lines[self.line_segment_number]["start_longitude"] < route.lines[self.line_segment_number]["end_longitude"]:
      direction = "e"

    if not self.stopped:
      for stop in stops:
        if haversine_distance(self.latitude,
          self.longitude,
          stop.latitude,
          stop.longitude) < 50 / 1000.0 and direction == stop.direction and (self.next_stop_id == -1 or self.next_stop_id == stop.stop_id):

            self.stopped = True
            if self.next_stop_id == -1:
              self.next_stop_id = stop.stop_id
            self.stop_time = BUS_STOP_TIME / float(SPEED_FACTOR)

      distance_to_go = SPEED_FACTOR * dt * self.speed / 3600.0
    
      while distance_to_go > 0:
        segment_remaining_distance = haversine_distance(self.latitude,
            self.longitude,
            route.lines[self.line_segment_number]["end_latitude"],
            route.lines[self.line_segment_number]["end_longitude"])

        if segment_remaining_distance > distance_to_go:
          # move the bus along the segment for the appropriate distance
          fractional_distance = distance_to_go / segment_remaining_distance
          self.latitude += fractional_distance * (route.lines[self.line_segment_number]["end_latitude"] - self.latitude)
          self.longitude += fractional_distance * (route.lines[self.line_segment_number]["end_longitude"] - self.longitude)
          distance_to_go = 0
        else:
          distance_to_go -= segment_remaining_distance
          next_segment_number = self.line_segment_number + 1
          if next_segment_number >= len(route.lines):
            next_segment_number = 0
          self.line_segment_number = next_segment_number
          self.latitude = route.lines[self.line_segment_number]["start_latitude"]
          self.longitude = route.lines[self.line_segment_number]["start_longitude"]
    else:
      self.stop_time -= dt
      if self.stop_time <= 0:
        self.stop_time = 0
        self.stopped = False
        self.next_stop_id += 1
        if self.next_stop_id >= len(stops):
          self.next_stop_id = 0


  def draw(self, x, y, zoom_level):
    sign_scale = zoom_level / 20.0
    if sign_scale > 1:
      sign_scale = 1
    bus_image.blit(x - 12 * sign_scale, y - 12 * sign_scale, width = (24 * sign_scale), height = (24 * sign_scale))
    if zoom_level > 14:
      self.label.x = x + 12
      self.label.y = y - 12
      self.label.text = "%d,%d,%d" % (self.id, self.passenger_count, self.next_stop_id)
      if self.stopped:
        self.label.text += ",s%.2f" % self.stop_time
      self.label.draw()


def threaded_data_send(passenger):
  #print "%s pings" % passenger.user_id
  
  # jitter, about 10 feet, totally random no bell curve
  latitude = passenger.latitude - 1.0 / 10000.0 + random.random() * 2.0 / 10000.0
  longitude = passenger.longitude - 1.0 / 10000.0 + random.random() * 2.0 / 10000.0

  data = {
    "latitude": latitude,
    "longitude": longitude,
    "speed": passenger.speed,
    "heading": 23.0,
    "user_id": passenger.user_id,
    "route_slug": "LA-720",
    "timestamp": datetime.datetime.strftime(datetime.datetime.now(),"%m/%d/%y %H:%M:%S"),
    "secret": "OHWEGFNOSUDGVWPSLSDERETU"
  }

  try:
    req = urllib2.Request("http://busby.lukemizuhashi.com/api/v0.0/busby/data")
    req.add_header("Content-Type", "application/json")

    response = urllib2.urlopen(req, json.dumps(data))
  except Exception, e:
    print "wooha, ", datetime.datetime.strftime(datetime.datetime.now(),"%m/%d/%y %H:%M:%S"), e


class Passenger(object):

  def __init__(self, latitude, longitude, target_id):
    self.latitude = latitude
    self.longitude = longitude
    self.old_latitude = latitude
    self.old_longitude = longitude
    self.mode = MODE_TARGET
    self.transmitting = True
    self.target_id = target_id
    self.place_id = -1
    self.user_id = user_id_generator()
    self.last_ping = -1
    self.speed = -1
    self.last_ping_latitude = latitude
    self.last_ping_longitude = longitude
    self.egress_target_latitude = -1
    self.egress_target_longitude = -1


  def update(self, app, dt):
    self.old_longitude = self.longitude
    self.old_latitude = self.latitude

    if self.mode == MODE_TARGET:
      target_lat = app.stop_dict[self.target_id].latitude
      target_lng = app.stop_dict[self.target_id].longitude
      # remember, haversine returns a value in km
      if haversine_distance(self.latitude, self.longitude, target_lat, target_lng) > 3.0 / 1000.0:
        if self.latitude > target_lat:
          self.latitude -= 0.1/10000.0
        elif self.latitude < target_lat:
          self.latitude += 0.1/10000.0

        if self.longitude > target_lng:
          self.longitude -= 0.1/10000.0
        elif self.longitude < target_lng:
          self.longitude += 0.1/10000.0

        time_diff = time.time() - self.last_ping
        #time_diff = diff.seconds + diff.microseconds/1E6
        self.speed = haversine_distance(self.latitude, self.longitude, self.last_ping_latitude, self.last_ping_longitude) * 3600.0 / (time_diff * SPEED_FACTOR)
      else:
        self.mode = MODE_STOP
        app.stop_dict[self.target_id].passenger_count += 1
        self.place_id = self.target_id
        app.total_walking -= 1
        app.total_waiting += 1
        if  self.place_id == 37:
          self.target_id = random.randint(0, 18)
        elif self.place_id < 18:
          self.target_id = random.randint(self.place_id + 1, 18)
        elif self.place_id < 37:
          self.target_id = random.randint(self.place_id + 1, 37)
    elif self.mode == MODE_STOP:
      self.speed = 0
      for bus in app.buses:
        if bus.stopped and bus.next_stop_id == self.place_id:
          app.stop_dict[self.place_id].passenger_count -= 1
          self.place_id = bus.id
          bus.passenger_count += 1
          app.total_waiting -= 1
          app.total_riding += 1
          self.mode = MODE_BUS
          break
    elif self.mode == MODE_BUS:
      my_bus = app.bus_dict[self.place_id]
      self.longitude = my_bus.longitude
      self.latitude = my_bus.latitude
      time_diff = time.time() - self.last_ping
      self.speed = haversine_distance(self.latitude, self.longitude, self.last_ping_latitude, self.last_ping_longitude) * 3600.0 / (time_diff * SPEED_FACTOR)
      #self.speed = haversine_distance(self.latitude, self.longitude, self.old_latitude, self.old_longitude)
      if my_bus.stopped and my_bus.next_stop_id == self.target_id:
        my_bus.passenger_count -= 1
        app.total_riding -= 1
        self.mode = MODE_EGRESS
        self.egress_target_latitude = self.latitude - 10.0 / 10000.0 + random.random() * 20.0 / 10000.0
        self.egress_target_longitude = self.longitude - 10.0 / 10000.0 + random.random() * 20.0 / 10000.0
    elif self.mode == MODE_EGRESS:

      # remember, haversine returns a value in km
      if haversine_distance(self.latitude, self.longitude, self.egress_target_latitude, self.egress_target_longitude) > 3.0 / 1000.0:
        if self.latitude > self.egress_target_latitude:
          self.latitude -= 0.1/10000.0
        elif self.latitude < self.egress_target_latitude:
          self.latitude += 0.1/10000.0

        if self.longitude > self.egress_target_longitude:
          self.longitude -= 0.1/10000.0
        elif self.longitude < self.egress_target_longitude:
          self.longitude += 0.1/10000.0

        time_diff = time.time() - self.last_ping
        self.speed = haversine_distance(self.latitude, self.longitude, self.last_ping_latitude, self.last_ping_longitude) * 3600.0 / (time_diff * SPEED_FACTOR)
      else:
        app.total_finished += 1
        if not self.transmitting:
          app.total_not_transmitting -= 1
        self.mode = MODE_FINISHED

    # send a location to the real server!
    if self.last_ping == -1 or time.time() - self.last_ping > PASSENGER_PING_RATE:
      if SEND_GPS and self.transmitting:
        self.last_ping_latitude = self.latitude
        self.last_ping_longitude = self.longitude
        thread = threading.Thread(target = threaded_data_send, args = (self, ))
        thread.start()
        self.last_ping = time.time()

    # potentially stop or start transmitting
    dice = random.random()
    if self.transmitting and dice < STOP_TRANSMITTING_RATE:
      self.transmitting = False
      app.total_not_transmitting += 1
    elif not self.transmitting and dice < RESTART_TRANSMITTING_RATE:
      self.transmitting = True
      app.total_not_transmitting -= 1

  def draw(self, x, y, zoom_level):
    sign_scale = zoom_level / 20.0
    if sign_scale > 1:
      sign_scale = 1
    draw_image = passenger_image
    if self.mode == MODE_EGRESS:
      draw_image = passenger_egress_image
    draw_image.blit(x - 12 * sign_scale, y - 12 * sign_scale, width = (24 * sign_scale), height = (24 * sign_scale))
    #if zoom_level > 14:
      #self.label.x = x + 12
      #self.label.y = y - 12
      #self.label.text = "%d,%d" % (self.stop_id, len(self.passengers))
      #self.label.draw()


class Stop(object):

  def __init__(self, latitude, longitude, stop_id, direction):
    self.latitude = latitude
    self.longitude = longitude
    self.stop_id = stop_id
    self.direction = direction
    #self.passengers = []
    self.passenger_count = 0
    self.label = Label(10, 570, "Arial", 10, "10,4", (0.8, 0.3, 0.3, 1.0))

  def draw(self, x, y, zoom_level):
    sign_scale = zoom_level / 20.0
    if sign_scale > 1:
      sign_scale = 1
    stop_image.blit(x - 12 * sign_scale, y - 12 * sign_scale, width = (24 * sign_scale), height = (24 * sign_scale))
    if zoom_level > 14:
      self.label.x = x + 12
      self.label.y = y - 12
      self.label.text = "%d,%d" % (self.stop_id, self.passenger_count)
      self.label.draw()


class BusbySimulator(pyglet.window.Window):
  
  def __init__(self, width, height):
    pyglet.window.Window.__init__(self, width, height)

    pyglet.font.add_file("Arial.ttf")

    self.route = Route("LA-720")
    self.stops = []
    self.stop_dict = {}
    for stop in self.route.stops:
      new_stop = Stop(stop["latitude"], stop["longitude"], stop["stop_id"], stop["direction"])
      self.stops.append(new_stop)
      self.stop_dict[stop["stop_id"]] = new_stop
    # for stop in self.stops:
    #   if stop.stop_id == 30 or stop.stop_id == 7:
    #     print stop.stop_id
    #     print stop.longitude
    #     print stop.latitude
    self.zoom_level = 10
    self.viewpoint_latitude = 34.05
    self.viewpoint_longitude = -118.46
    self.last_passenger_drip = time.time()
    self.passengers = []

    self.buses = []
    self.bus_dict = {}
    num_buses = 0
    while(num_buses < MAX_BUSES):
      bus = Bus(num_buses, self.stops, self.route)
      self.buses.append(bus)
      self.bus_dict[num_buses] = bus
      num_buses += 1

    self.total_walking = 0
    self.total_waiting = 0
    self.total_riding = 0
    self.total_finished = 0
    self.total_not_transmitting = 0

    self.walkingLabel = Label(100, HEIGHT - 60, "Arial", 20, "", (0.0, 0.0, 0.000, 1.0))
    self.waitingLabel = Label(100, HEIGHT - 140, "Arial", 20, "", (0.0, 0.0, 0.000, 1.0))
    self.ridingLabel = Label(100, HEIGHT - 220, "Arial", 20, "", (0.0, 0.0, 0.000, 1.0))
    self.finishedLabel = Label(100, HEIGHT - 300, "Arial", 20, "", (0.0, 0.0, 0.000, 1.0))
    self.notTransmittingLabel = Label(100, HEIGHT - 380, "Arial", 20, "", (0.0, 0.0, 0.000, 1.0))
    

  def update(self, dt):
    for bus in self.buses:
      bus.update(self.route, self.stops, dt)

    keep_passengers = []

    for passenger in self.passengers:
      passenger.update(self, dt)
      if passenger.mode != MODE_FINISHED:
        keep_passengers.append(passenger)

    self.passengers = keep_passengers

    if time.time() - self.last_passenger_drip > PASSENGER_DRIP_RATE:
      stop_target = random.randint(0, len(self.stops) - 1)
      #stop_target = 7
      latitude = self.stop_dict[stop_target].latitude - 5.0 / 10000.0 + random.random() * 10.0 / 10000.0
      longitude = self.stop_dict[stop_target].longitude - 5.0 / 10000.0 + random.random() * 10.0 / 10000.0
      passenger = Passenger(latitude, longitude, stop_target)
      self.passengers.append(passenger)
      self.last_passenger_drip = time.time()
      self.total_walking += 1
    
  def on_draw(self):
    glClearColor(0.8, 0.8, 0.8, 1.0)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    Start2DDraw(self.width,self.height)

    #self.testLabel.draw()

    scale_factor = math.pow(25, math.log(self.zoom_level))

    # draw route
    glLineWidth(2)
    glColor3f(0.6, 1.0, 0.6)
    glBegin(GL_LINES)
    for line in self.route.lines:
      glVertex2f(WIDTH / 2 + (line["start_longitude"] - self.viewpoint_longitude) * scale_factor, HEIGHT / 2 + (line["start_latitude"] - self.viewpoint_latitude) * scale_factor)
      glVertex2f(WIDTH / 2 + (line["end_longitude"] - self.viewpoint_longitude) * scale_factor, HEIGHT / 2 + (line["end_latitude"] - self.viewpoint_latitude) * scale_factor)
    glEnd()

    glColor3f(1.0, 1.0, 1.0)

    # draw stops
    for stop in self.stops:
      x = WIDTH / 2 + (stop.longitude - self.viewpoint_longitude) * scale_factor
      y = HEIGHT / 2 + (stop.latitude - self.viewpoint_latitude) * scale_factor
      stop.draw(x, y, self.zoom_level)

    # draw outstanding passengers
    for passenger in self.passengers:
      if passenger.mode == MODE_TARGET or passenger.mode == MODE_EGRESS:
        x = WIDTH / 2 + (passenger.longitude - self.viewpoint_longitude) * scale_factor
        y = HEIGHT / 2 + (passenger.latitude - self.viewpoint_latitude) * scale_factor
        passenger.draw(x, y, self.zoom_level)

    # draw buses
    for bus in self.buses:
      x = WIDTH / 2 + (bus.longitude - self.viewpoint_longitude) * scale_factor
      y = HEIGHT / 2 + (bus.latitude - self.viewpoint_latitude) * scale_factor
      bus.draw(x, y, self.zoom_level)

    info_walking.blit(20, HEIGHT - 80, height=64, width=64)
    info_waiting.blit(20, HEIGHT - 160, height=64, width=64)
    info_riding.blit(20, HEIGHT - 240, height=64, width=64)
    info_finished.blit(20, HEIGHT - 320, height=64, width=64)
    info_not_transmitting.blit(20, HEIGHT - 400, height=64, width=64)

    self.walkingLabel.text = "%d" % self.total_walking
    self.walkingLabel.draw()
    self.waitingLabel.text = "%d" % self.total_waiting
    self.waitingLabel.draw()
    self.ridingLabel.text = "%d" % self.total_riding
    self.ridingLabel.draw()
    self.finishedLabel.text = "%d" % self.total_finished
    self.finishedLabel.draw()
    self.notTransmittingLabel.text = "%d" % self.total_not_transmitting
    self.notTransmittingLabel.draw()


    End2DDraw()
    

  def on_key_press(self, symbol, modifiers):
    if symbol == 113:
      exit(0)

    if symbol == 61:
      self.zoom_level += 1

    if symbol == 45:
      self.zoom_level -= 1

  def on_mouse_drag(self, x, y, dx, dy, symbol, modifiers):
    scale_factor = math.pow(25, math.log(self.zoom_level))

    self.viewpoint_longitude -= dx / scale_factor
    self.viewpoint_latitude -= dy / scale_factor


#make a window/app
busbySimulator = BusbySimulator(WIDTH, HEIGHT)
#give it access to keys
keys = key.KeyStateHandler()
busbySimulator.push_handlers(keys)

#schedule the update function to run 60 times a second.
clock.schedule_interval(busbySimulator.update, 1/60.0)

#start the app
pyglet.app.run()