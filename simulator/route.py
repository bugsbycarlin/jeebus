import json
import urllib2

class Route(object):

  def __init__(self, route_name):
    try:
      url = "http://busby.lukemizuhashi.com/api/v0.0/routecorder/" + route_name

      data = json.load(urllib2.urlopen(url))

      self.name = data["name"]
      self.region = data["region"]
      self.slug = data["slug"]

      self.stops = data["stops"]
      self.lines = data["lines"]

      self.data = data
    except Exception, e:
      print "Failed to load route data with exception:"
      print e
      exit(0)

  def json_data(self):
    return self.data