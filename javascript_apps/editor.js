
var map = null;
var stops = [];
var line_paths = [];
var current_line_path = null;
var current_line_drawing = null;
var stop_id = 0;
var circles = [];
var draw_lines = [];
var selected_circle = null;
var route = "";
var mode = "STOPS";



function initialize()
{
  var mapOptions = {
    // start map at Wilshire and Westwood near UCLA
    center: new google.maps.LatLng(34.058757, -118.443786),
    zoom: 15
  };
  
  map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

  map.setOptions({draggableCursor: "auto"});

  url = "http://busby.lukemizuhashi.com/api/v0.0/routecorder/" + $("#route_select").val();

  $.ajax({
    type: "GET",

    url: url,

    json: "callback",

    dataType: "json",

    success: function(response) {
      console.log(response);
      console.log("bees");

      stop_data = response["stops"];
      for(var j = 0; j < stop_data.length; j++)
      {
        sd = stop_data[j];
        stops[stop_id] = [sd["latitude"], sd["longitude"]];
        stop_id += 1;
      }

      for(var i = 0; i < stops.length; i++)
      {
        stop = stops[i];

        if(stop != null)
        {
          addCircle(stop, i);
        }
      }

      line_data = response["lines"];
      for(var j = 0; j < line_data.length; j++)
      {
        ld = line_data[j];
        
        line = [new google.maps.LatLng(ld["start_latitude"], ld["start_longitude"]), 
            new google.maps.LatLng(ld["end_latitude"], ld["end_longitude"])]; 
        line_paths.push(line)

        map_line = new google.maps.Polyline({
                path:line,
                strokeColor: "#88FF88",
                strokeOpacity: 1.0,
                strokeWeight: 2,
                editable: false,
              });
        map_line.setMap(map);
      }
      console.log(line_paths);

      $(document).on("keydown", function (e) {
          console.log(e.keyCode);
          if (e.which === 8) {
            e.preventDefault();

            if(mode == "STOPS" && selected_circle != null)
            {
              console.log(selected_circle);
              console.log(selected_circle.stop_id);
              stops[selected_circle.stop_id] = null;
              selected_circle.setMap(null);
              selected_circle = null;

              console.log(stops);
            }
          }
          else if(e.keyCode == 67)
          {
            e.preventDefault();

            if(mode == "LINES" && current_line_path.length > 1)
            {
              line_paths.push(current_line_path);

              if(current_line_drawing)
              {
                current_line_drawing.setMap(null);
              }

              new_line_drawing = new google.maps.Polyline({
                path:current_line_path,
                strokeColor: "#88FF88",
                strokeOpacity: 1.0,
                strokeWeight: 2,
                editable: false,
              });
              new_line_drawing.setMap(map);

              draw_lines.push(new_line_drawing);

              current_line_path = null;
            }
          }
          else if(e.keyCode == 90)
          {
            e.preventDefault();

            if(mode == "LINES" && current_line_path != null && current_line_path.length > 0)
            {
              current_line_path.pop();

              if(current_line_drawing)
              {
                current_line_drawing.setMap(null);
              }

              current_line_drawing = new google.maps.Polyline({
                path:current_line_path,
                strokeColor: "#FF8888",
                strokeOpacity: 1.0,
                strokeWeight: 2,
                editable: true,
              });
              current_line_drawing.setMap(map); 
            }
          }
      });    

      google.maps.event.addListener(map, 'click', function(ev) {
        if(mode == "STOPS")
        {
          clearSelected();
          selected_circle = null;
          stop = [ev.latLng.lat(), ev.latLng.lng()];

          addCircle(stop, stop_id);

          stops[stop_id] = stop;
          stop_id += 1;
        }
        else if(mode == "LINES")
        {
          console.log(ev.latLng.lat() + " " + ev.latLng.lng());
          if(current_line_path == null)
          {
            current_line_path = [ev.latLng];
          }
          else
          {
            current_line_path.push(ev.latLng);

            if(current_line_drawing)
            {
              current_line_drawing.setMap(null);
            }

            current_line_drawing = new google.maps.Polyline({
              path:current_line_path,
              strokeColor: "#FF8888",
              strokeOpacity: 1.0,
              strokeWeight: 2,
              editable: true,
            });
            current_line_drawing.setMap(map);
          }
        }
      });

      $("#mode_select").change(function() {
        mode = $("#mode_select").val();

        if(mode == "LINES")
        {
          clearSelected();
          selected_circle = null;
          for(var i = 0; i < circles.length; i++)
          {
            circle = circles[i];
            circle.setOptions({draggable: false});
          }
          map.setOptions({draggableCursor: "crosshair"});
        }
        else if(mode == "STOPS")
        {
          for(var i = 0; i < circles.length; i++)
          {
            circle = circles[i];
            circle.setOptions({draggable: true});
          }
          map.setOptions({draggableCursor: "auto"});
        }

      });

      $("#save_button").click(function() {
        console.log("Saving...");
          url = "http://busby.lukemizuhashi.com/api/v0.0/routecorder/" + $("#route_select").val() + "/save_stops"

          save_lines = makeSaveLines();

          $.ajax({
            type: "POST",
            contentType : "application/json",
            url: url,
            dataType: "json",
            data: JSON.stringify({"stops": stops, "lines": save_lines, "secret": "OHWEGFNOSUDGVWPSLSDERETU"}),
            success: function(response) {
              console.log(response);
            }
          });
      });
    }
  });
}

function makeSaveLines()
{
  save_lines = [];

  for(var i = 0; i < line_paths.length; i++)
  {
    path = line_paths[i];
    for(var j = 0; j < path.length - 1; j++)
    {
      save_lines.push([path[j].lat(), path[j].lng(), path[j+1].lat(), path[j+1].lng()]);
    }
  }

  return save_lines;
}

function addCircle(stop, stop_id)
{
  var circle = new google.maps.Circle({
    strokeColor: "#000000",
    strokeOpacity: 1.0,
    strokeWeight: 2,
    fillColor: "#9B9BC8",
    fillOpacity: 0.70,
    map: map,
    center: new google.maps.LatLng(stop[0], stop[1]),
    draggable: true,
    radius: 15
  });

  // closure to preserve the correct variable attachment for circle
  (function(circle, stop_id){
    console.log(stop_id);
    circle.stop_id = stop_id;
    circles.push(circle);
    google.maps.event.addListener(circle, 'drag', function() {
      clearSelected();
      circle.setOptions({fillColor: "#C89B9B"});
      selected_circle = circle;
      pos = circle.getCenter();
      stops[circle.stop_id] = [pos.lat(), pos.lng()]
    });

    google.maps.event.addListener(circle, 'click', function() {
      if(mode == "STOPS")
      {
        clearSelected();
        circle.setOptions({fillColor: "#C89B9B"});
        selected_circle = circle;
      }
    });
  })(circle, stop_id);
}

function clearSelected()
{
  if (selected_circle != null)
  {
    selected_circle.setOptions({fillColor: "#9B9BC8"});
  }
}