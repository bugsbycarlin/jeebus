
var map = null;
var stops = [];
var stop_numbers = [];
var line_paths = [];

var stop_id = 0;
var draw_lines = [];

var bus_squares = [];
var bus_radius = 30;

function initialize()
{
  var mapOptions = {
    // start map at Wilshire and Westwood near UCLA
    center: new google.maps.LatLng(34.058757, -118.443786),
    zoom: 15
  };
  
  map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

  map.setOptions({draggableCursor: "auto"});

  url = "http://localhost:5000/api/v0.0/simulator";

  $.ajax({
    url: url,

    jsonp: "callback",

    dataType: "jsonp",

    success: function(response) {
      console.log(response);
      //console.log("bees");

      rider_data = response["stop_riders"];
      for(var i = 0; i < rider_data.length; i++)
      {
        rider = rider_data[i];
        stop_numbers[rider] += 1;
      }

      console.log("now here");
      stop_data = response["stops"];
      console.log(stop_data);
      for(var i = 0; i < stop_data.length; i++)
      {
        stop = stop_data[i];
        console.log(stop);
        marker = addStop(stop, stop_numbers[i]);
        stops.push(marker);
        stop_numbers[i] = 0;
      }

      route = response["route"];
      line_data = route["lines"];
      for(var j = 0; j < line_data.length; j++)
      {
        ld = line_data[j];
        
        line = [new google.maps.LatLng(ld["start_latitude"], ld["start_longitude"]), 
            new google.maps.LatLng(ld["end_latitude"], ld["end_longitude"])]; 
        line_paths.push(line)

        map_line = new google.maps.Polyline({
                path:line,
                strokeColor: "#88FF88",
                strokeOpacity: 1.0,
                strokeWeight: 2,
                editable: false,
                zIndex: 10
              });
        map_line.setMap(map);
      }
      //console.log(line_paths);


      bus_data = response["buses"];
      for(var i = 0; i < bus_data.length; i++)
      {
        bus = bus_data[i];
        //console.log(bus);

        center = new google.maps.LatLng(bus["latitude"], bus["longitude"])

        square_bounds = new google.maps.Circle({center: center, radius: bus_radius}).getBounds();

        var bus_square = new google.maps.Rectangle({
          strokeColor: '#000000',
          strokeOpacity: 1.0,
          strokeWeight: 2,
          fillColor: '#FFFF95',
          fillOpacity: 0.80,
          map: map,
          bounds: square_bounds,
          zIndex: 100
        });
        bus_squares.push(bus_square);

        map.setOptions({center: center});
      }

      $(document).on("keydown", function (e) {
        // empty for now 
      });    

      google.maps.event.addListener(map, 'click', function(ev) {
        // empty for now
      });

      //setInterval(updateSimulation, 1000 / 4.0);
      setInterval(updateStopNumbers, 1000);
    }
  });
}

function updateStopNumbers()
{

}

function updateSimulation()
{
  new_bus_squares = [];
  $.ajax({
    url: "http://localhost:5000/api/v0.0/simulator",

    jsonp: "callback",

    dataType: "jsonp",

    success: function(response) {
      //console.log("here we goooo");
      bus_data = response["buses"];

      for(var i = 0; i < bus_data.length; i++)
      {
        bus = bus_data[i];

        center = new google.maps.LatLng(bus["latitude"], bus["longitude"])

        square_bounds = new google.maps.Circle({center: center, radius: bus_radius}).getBounds();

        var bus_square = new google.maps.Rectangle({
          strokeColor: '#DDDD75',
          strokeOpacity: 1.0,
          strokeWeight: 2,
          fillColor: '#FFFF95',
          fillOpacity: 0.80,
          map: map,
          bounds: square_bounds,
          zIndex: 100
        });
        new_bus_squares.push(bus_square);
      }

      for(var i = 0; i < bus_squares.length; i++)
      {
        bus_square = bus_squares[i];
        bus_square.setMap(null);
        bus_square = null;
      }

      bus_squares = new_bus_squares;

      rider_data = response["riders"];
      stop_data = response["stops"];

      // keep this code, it does rider numbers
      // var new_stop_numbers = [];
      // for(var i = 0; i < stop_data.length; i++)
      // {
      //   new_stop_numbers[i] = 0;
      // }
      // for(var i = 0; i < rider_data.length; i++)
      // {
      //   rider = rider_data[i];
      //   //console.log(rider["stop_num"]);
      //   new_stop_numbers[rider["stop_num"]] += 1;
      // }

      // for(var i = 0; i < stop_data.length; i++)
      // {
      //   marker_number = Math.min(100, new_stop_numbers[i])
      //   if (marker_number != stop_numbers[i])
      //   {
      //     stops[i].setOptions({icon:"Art/number_" + marker_number + ".png"});
      //   }
      // }
      // stop_numbers = new_stop_numbers;

    }
  });

}

function addStop(stop, num_riders)
{
  // var circle = new google.maps.Circle({
  //   strokeColor: "#7B7BA8",
  //   strokeOpacity: 1.0,
  //   strokeWeight: 2,
  //   fillColor: "#9B9BC8",
  //   fillOpacity: 0.70,
  //   zIndex: 20,
  //   map: map,
  //   center: new google.maps.LatLng(stop[0], stop[1]),
  //   draggable: true,
  //   radius: 15
  // });

  //console.log("Stop");
  //console.log(stop);

  // this is how to print number of people
  console.log(stop_numbers);
  marker_number = Math.min(100, num_riders)
  //marker_number = stop["stop_id"];

  var marker = new google.maps.Marker({
            position: new google.maps.LatLng(stop["latitude"], stop["longitude"]),
            icon:"Art/number_" + marker_number + ".png",
            map: map,
            zIndex: 20
  });

  return marker;


  // closure to preserve the correct variable attachment for circle
  // (function(circle, stop_id){
  //   console.log(stop_id);
  //   circle.stop_id = stop_id;
  //   circles.push(circle);
    // google.maps.event.addListener(circle, 'drag', function() {
    //   clearSelected();
    //   circle.setOptions({fillColor: "#C89B9B"});
    //   selected_circle = circle;
    //   pos = circle.getCenter();
    //   stops[circle.stop_id] = [pos.lat(), pos.lng()]
    // });

    // google.maps.event.addListener(circle, 'click', function() {
    //   if(mode == "STOPS")
    //   {
    //     clearSelected();
    //     circle.setOptions({fillColor: "#C89B9B"});
    //     selected_circle = circle;
    //   }
    // });
  // })(circle, stop_id);
}
