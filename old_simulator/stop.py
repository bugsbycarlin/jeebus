import random

from mongoengine import *

from route import *
from rider import *
from settings import *
from utilities import *

connect("simulator")

class Stop(Document):
  latitude = FloatField(required = True)
  longitude = FloatField(required = True)
  stop_id = IntField(required = True)

  def make_public(self):
    return {
      "latitude": self.latitude,
      "longitude": self.longitude,
      "stop_id": self.stop_id
    }

