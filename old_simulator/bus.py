import random

from mongoengine import *

from route import *
from rider import *
from settings import *
from utilities import *


connect("simulator")

class Bus(Document):
  speed = FloatField(required = True)
  latitude = FloatField(required = True)
  longitude = FloatField(required = True)
  line_segment_number = IntField(required = True)
  stopped = BooleanField(required = True, default = False)
  stop_time = FloatField(required = True, default = 0)
  next_stop_id = IntField(required = True, default = 0)

  def init(self, route):
    self.speed = random.randint(int(BUS_SPEED * 0.9), int(BUS_SPEED * 1.1))
    self.line_segment_number = random.randint(0, len(route.lines) - 1)
    self.latitude = route.lines[self.line_segment_number]["start_latitude"]
    self.longitude = route.lines[self.line_segment_number]["start_longitude"]
    self.next_stop_id = -1

  def update(self, route, dt):
    if not self.stopped:
      distance_to_go = SPEED_FACTOR * dt * self.speed / 3600.0

      while distance_to_go > 0:
        segment_remaining_distance = haversine_distance(self.latitude,
            self.longitude,
            route.lines[self.line_segment_number]["end_latitude"],
            route.lines[self.line_segment_number]["end_longitude"])

        if segment_remaining_distance > distance_to_go:
          # move the bus along the segment for the appropriate distance
          fractional_distance = distance_to_go / segment_remaining_distance
          self.latitude += fractional_distance * (route.lines[self.line_segment_number]["end_latitude"] - self.latitude)
          self.longitude += fractional_distance * (route.lines[self.line_segment_number]["end_longitude"] - self.longitude)
          distance_to_go = 0
        else:
          distance_to_go -= segment_remaining_distance
          next_segment_number = self.line_segment_number + 1
          if next_segment_number >= len(route.lines):
            next_segment_number = 0
          self.line_segment_number = next_segment_number
          self.latitude = route.lines[self.line_segment_number]["start_latitude"]
          self.longitude = route.lines[self.line_segment_number]["start_longitude"]

    else:
      self.stop_time -= dt
      #print self.stop_time
      if self.stop_time <= 0:
        self.stop_time = 0
        self.stopped = False
        self.next_stop_id += 1
        if self.next_stop_id >= 38:
          self.next_stop_id = 0
    #self.save()
