import math

EARTH_RADIUS = 6371 #km

# calculate great circle distance between two lat/lng pairs
def haversine_distance(lat1, lng1, lat2, lng2):
  lat1 = math.radians(lat1)
  lng1 = math.radians(lng1)
  lat2 = math.radians(lat2)
  lng2 = math.radians(lng2)

  dlat = lat2 - lat1
  dlng = lng2 - lng1

  a = math.sin(dlat / 2.0) ** 2 + math.cos(lat1) * math.cos(lat2) * (math.sin(dlng / 2.0) ** 2)
  c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
  d = c * EARTH_RADIUS

  return d