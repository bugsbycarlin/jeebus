from mongoengine import *

connect("simulator")

class Rider(Document):
  latitude = FloatField(required = True)
  longitude = FloatField(required = True)
  stop_or_bus = BooleanField(required = True)
  stop_num = IntField(required = True)
  goal_num = IntField(required = True)

  #riders = ListField(db.EmbeddedDocumentField("Rider"))

  def make_public(self):
    return {
      "latitude": self.latitude,
      "longitude": self.longitude,
      "stop_or_bus": self.stop_or_bus,
      "stop_num": self.stop_num,
      "goal_num": self.goal_num
    }
