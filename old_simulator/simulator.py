import random
import time

from settings import *
from bus import *
from rider import *
from stop import *
from route import *
from utilities import *

# Get the route (from the main server)
route = Route("LA-720")


# # cheeky test calculation of haversine
# total_distance = 0
# for line in route.lines:
#   x = haversine_distance(line["start_latitude"],
#       line["start_longitude"],
#       line["end_latitude"],
#       line["end_longitude"])
#   print "Segment distance: %r" % x
#   total_distance += x
# print "Total: %r km" % total_distance


# one time thing; disable whenever there are already stops
# for stop_data in route.stops:
#   new_stop = Stop()
#   new_stop.latitude = stop_data["latitude"]
#   new_stop.longitude = stop_data["longitude"]
#   new_stop.save()
# print "stops done"
# exit(0)


#one time thing; assign stop ids to the stops
# i = 0
# for stop in Stop.objects():
#   stop.stop_id = i
#   stop.save()
#   i += 1
# exit(0)


# one time thing: clear all riders
# for stop in Stop.objects():
#   stop.riders = []
#   stop.save()


num_stops = Stop.objects().count()

mod_count = 0

# make some buses
num_buses = Bus.objects().count()
while(num_buses < MAX_BUSES):
  print "Creating bus."
  bus = Bus()

  bus.init(route)

  bus.save()
  num_buses += 1


time_since_riders = time.time()

def main_loop(dt):
  global time_since_riders
  for bus in Bus.objects():
    bus.update(route, dt)
    #print dt
    #print "Bus is at segment: %d, lat: %f, lng: %f" % (bus.line_segment_number, bus.latitude, bus.longitude)

    for i in range(0, 38):
    #for stop in Stop.objects():
      stop = None
      try:
        stop = Stop.objects.get(stop_id = i)
      except Exception:
        pass
      if stop:
        if haversine_distance(bus.latitude,
            bus.longitude,
            stop.latitude,
            stop.longitude) < 30 / 1000.0 and (stop.stop_id == -1 or bus.next_stop_id == i):

          bus.stopped = True
          if bus.next_stop_id == -1:
            bus.next_stop_id = stop.stop_id
          bus.stop_time = BUS_STOP_TIME / float(SPEED_FACTOR)
          
          
    if bus.stopped:
      stop_riders = Rider.objects.update(pull__x={"stop_num": bus.next_stop_id})

      #print len(stop_riders)

    bus.save()
    



  if time.time() - time_since_riders > 1:
    #new_rider_num = NEW_RIDERS_PER_SECOND * SPEED_FACTOR
    new_rider_num = 40
    new_riders = []
    print "start list"
    list_start_time = time.time()
    for i in range(0, int(new_rider_num)):
      # make a new rider!
      stop_num = random.randint(0, num_stops - 1)
      goal_num = random.randint(0, num_stops - 1)
      if goal_num == stop_num:
        goal_num = stop_num + 1
        if goal_num >= num_stops:
          goal_num = 0
      stop = Stop.objects.get(stop_id = stop_num)
      rider = Rider()
      rider.stop_num = stop_num
      rider.goal_num = goal_num
      rider.latitude = stop["latitude"]
      rider.longitude = stop["longitude"]
      new_riders.append(rider)
    print Rider.objects.count()
    print "stop list"
    list_time = time.time() - list_start_time
    print list_time
      #rider.save()
    #print "made list"
    Rider.objects.insert(new_riders)
    print "saved"
    #print "wrote list"
    time_since_riders = time.time()


last_check_time = time.time()
finished = False

while not finished:
  current_time = time.time()
  if current_time - last_check_time >= 1/30.0:

    main_loop(current_time - last_check_time)

    last_check_time = time.time()

    word = open("interrupt.txt").read()
    if "quit" in word:
      finished = True

