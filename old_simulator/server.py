from flask import Flask
from flask import jsonify, request, current_app
from flask.ext.mongoengine import MongoEngine
from functools import wraps

from route import *
from stop import *
from rider import *

# Get the route (from the main server)
my_route = Route("LA-720")

app = Flask(__name__)

app.config["MONGODB_SETTINGS"] = {"DB": "simulator"}

db = MongoEngine(app)

class Bus(db.Document):
  speed = db.FloatField(required = True)
  latitude = db.FloatField(required = True)
  longitude = db.FloatField(required = True)
  line_segment_number = db.IntField(required = True)

  def make_public(self):
    return {
      "speed": self.speed,
      "latitude": self.latitude,
      "longitude": self.longitude,
    }

# to use: curl -i http://localhost:5000/api/v0.0/simulator

# https://gist.github.com/farazdagi/1089923
# but unnecessary
def support_jsonp(f):
    """Wraps JSONified output for JSONP"""
    @wraps(f)
    def decorated_function(*args, **kwargs):
        callback = request.args.get('callback', False)
        if callback:
            content = str(callback) + '(' + str(f().data) + ')'
            return current_app.response_class(content, mimetype='application/json')
        else:
            return f(*args, **kwargs)
    return decorated_function


@app.route("/api/v0.0/simulator/write", methods = ["POST"])
def write_simdata():
  


@app.route("/api/v0.0/simulator", methods = ["GET"])
@support_jsonp
def get_task():
  buses = Bus.objects()
  stops_objects = Stop.objects()
  riders = Rider.objects()

  bus_riders = {}
  stop_riders = {}
  for rider in riders:
    if rider.stop_num not in stop_riders:
      stop_riders[rider.stop_num] = 1
    else:
      stop_riders[rider.stop_num] += 1

  stops_data = {}
  for stop in stops_objects:
    stops_data[stop.stop_id] = stop.make_public()

  data = {"route": my_route.json_data(), 
      "buses": [bus.make_public() for bus in buses],
      "stops": stops_data,
      "stop_riders": stop_riders,
      "bus_riders": bus_riders}

  return jsonify(data)

if __name__ == "__main__":
  app.run(debug = True, host = "0.0.0.0")
