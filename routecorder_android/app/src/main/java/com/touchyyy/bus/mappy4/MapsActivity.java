package com.touchyyy.bus.mappy4;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MapsActivity extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    private boolean recording;

    public class PointInfo {
        public double lat;
        public double lon;
        public String route;
        public boolean bus_stop;

        public PointInfo(double lat, double lon, String route, boolean bus_stop)
        {
            this.lat = lat;
            this.lon = lon;
            this.route = route;
            this.bus_stop = bus_stop;
        }
    }

    public class UpdateRouteTask extends AsyncTask<PointInfo, Void, Double> {

        protected Double doInBackground(PointInfo... points) {
            InputStream inputStream = null;
            String result = "";
            try {

                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                String url = "http://busby.lukemizuhashi.com/api/v0.0/routecorder/" + points[0].route + "/add_point";

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(url);

                String json = "";

                // 3. build jsonObject
                // eg "latitude":0.34532200, "longitude":0.9239293929, "bus_stop":false, "secret":"OHWEGFNOSUDGVWPSLSDERETU"
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("latitude", points[0].lat);
                jsonObject.accumulate("longitude", points[0].lon);
                jsonObject.accumulate("bus_stop", points[0].bus_stop);
                jsonObject.accumulate("secret", "OHWEGFNOSUDGVWPSLSDERETU");

                // 4. convert JSONObject to JSON to String
                json = jsonObject.toString();

                // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                // ObjectMapper mapper = new ObjectMapper();
                // json = mapper.writeValueAsString(person);

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the content
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

                Log.d("Response", result);

            } catch (Exception e) {
                Log.d("InputStream", e.toString());
            }

            return 2.2222;
        }
    }

    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public class BusbyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location loc) {

            if (recording) {
                LatLng update_circle = new LatLng(loc.getLatitude(), loc.getLongitude());

                Circle circle = mMap.addCircle(new CircleOptions()
                        .center(update_circle)
                        .radius(10)
                        .strokeColor(Color.BLACK)
                        .fillColor(Color.argb(180, 200, 155, 155)));

                Spinner routeSpinner = (Spinner)findViewById(R.id.routeSpinner);
                String route = routeSpinner.getSelectedItem().toString();

                new UpdateRouteTask().execute(new PointInfo(loc.getLatitude(), loc.getLongitude(), route, false));
            }
        }



        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setUpMapIfNeeded();

        recording = false;

        String[] routes = { "LA-720", "walk-sept-3-1", "walk-sept-3-2", "walk-sept-3-3" };

        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            routes);

        Spinner routeSpinner =
            (Spinner)findViewById(R.id.routeSpinner);

        routeSpinner.setAdapter(stringArrayAdapter);

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListener = new BusbyLocationListener();
        //locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0.0f, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 5.0f, locationListener);
    }

    public void busStopButtonClick(View e) {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Location loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        LatLng update_circle = new LatLng(loc.getLatitude(), loc.getLongitude());

        Circle circle = mMap.addCircle(new CircleOptions()
                .center(update_circle)
                .radius(10)
                .strokeColor(Color.BLACK)
                .fillColor(Color.argb(180, 155, 155, 200)));

        Spinner routeSpinner = (Spinner)findViewById(R.id.routeSpinner);
        String route = routeSpinner.getSelectedItem().toString();

        new UpdateRouteTask().execute(new PointInfo(loc.getLatitude(), loc.getLongitude(), route, true));
    }

    public void recordButtonClick(View e){
        if (recording) {
            recording = false;
            Button recordButton = (Button)findViewById(R.id.recordButton);
            recordButton.setText("Record");
            recordButton.setBackgroundColor(Color.LTGRAY);
        }
        else
        {
            recording = true;
            Button recordButton = (Button)findViewById(R.id.recordButton);
            recordButton.setText("Stop");
            recordButton.setBackgroundColor(Color.RED);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        //mMap.addMarker(new MarkerOptions().position(new LatLng(34.016759, -118.501234)).title("Marker"));

        LatLng nandarang = new LatLng(34.063794, -118.306001);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(nandarang, 18));

    }

}


